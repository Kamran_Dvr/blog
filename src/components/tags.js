import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

import Api from "../utility/api";
import { initialTags } from "../utility/localStorage";

function Tags() {
  const [data, setData] = useState([1]);

  useEffect(() => {
    initialTags();
    let tags = JSON.parse(window.localStorage.getItem("tags"));
    console.log(tags);
    setData(JSON.parse(window.localStorage.getItem("tags")));
    console.log(data);
  }, []);
  console.log(data);

  const renderItem = (item) => {
    return (
      <div>
        <Link to={`/users/useId/posts/${item.id}`}>{item.title}</Link>{" "}
      </div>
    );
  };

  const renderItems = () => {
    return data.map((item) => renderItem(item));
  };

  return <div className="tags"> {renderItems()}</div>;
}

export default Tags;
