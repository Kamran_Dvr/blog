import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

import Api from "../utility/api";

function Users() {
  const [data, setData] = useState([]);

  useEffect(() => {
    Api.get("users").then((res) => {
      console.log(res);
      console.log(res.data);
      setData(res.data);
    });
  }, []);

  const renderItem = (item) => {
    return (
      <div>
        <Link to={`users/${item.id}`}>{item.username}</Link>{" "}
        <Link to={`users/${item.id}/posts/`}>لیست مقالات</Link>
      </div>
    );
  };

  const renderItems = () => {
    return data.map((item) => renderItem(item));
  };

  return <div className="users">{renderItems()}</div>;
}

export default Users;
