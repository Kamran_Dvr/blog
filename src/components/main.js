import React from "react";
import { Switch, Route } from "react-router-dom";
import Users from "./users";
import Posts from "./posts";
import Post from "./post";
import Profile from "./profile";
import Tags from "./tags";
function Main() {
  return (
    <div className="nav">
      <Switch>
        <Route path="/" exact component={Users} />
        <Route path="/users/:userId" exact component={Profile} />
        <Route path="/users/:userId/posts" exact component={Posts} />
        <Route path="/users/:userId/posts/:postId" exact component={Post} />
        <Route path="/tags" exact component={Tags} />
      </Switch>
    </div>
  );
}

export default Main;
