import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import Api from "../utility/api";
import { handleTags } from "../utility/localStorage";

function Post(props) {
  const [post, setPost] = useState({});
  const [comments, setComments] = useState([]);

  useEffect(() => {
    Api.get(`posts/${props.match.params.postId}`).then((res) => {
      // console.log(res);
      // console.log(res.data);
      setPost(res.data);
    });
  }, []);
  useEffect(() => {
    Api.get(`posts/${props.match.params.postId}/comments`).then((res) => {
      console.log(res);
      console.log(res.data);
      setComments(res.data);
    });
  }, []);

  const renderComment = (item) => {
    return (
      <div>
        <h6>{item.name}</h6>
        
        <p>
          {item.body}
        </p>
        <p>{item.email}</p>
      </div>
    );
  };

  const renderComments = () => {
    return comments.map((item) => renderComment(item));
  };


  return (
    <div className="post">
      <h1>{post.title}</h1>
      <p>{post.body}</p>
      <button onClick={() => handleTags(post.id, post.title, post.userId)}>tag</button>
      <h1>Comments</h1>
      {renderComments()}
      <ToastContainer />
    </div>
  );
}

export default Post;
