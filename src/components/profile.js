import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

import Api from "../utility/api";

function Profile(props) {
  const [data, setData] = useState({company:{}, address:{}});

  useEffect(() => {
    Api.get(`users/${props.match.params.userId}`).then((res) => {
      console.log(res);
      console.log(res.data);
      setData(res.data);
    });
  }, []);
  return (
    <div className="profile">
      <h1>{data.name}</h1>
      <p>email: {data.email}</p>
      <p>website: {data.website}</p>
      <p>company: {data.company.name}</p>
      <p>city: {data.address.city}</p>
      <Link to={`${data.id}/posts`}>لیست مقالات</Link>
    </div>
  );
}

export default Profile;
