import React from "react";
import Nav from "./components/nav";
import Main from "./components/main";
import { BrowserRouter as Router } from "react-router-dom";

import "./App.css";

function App() {
  return (
    <Router>
      <div className="App">
        <Nav />
        <hr></hr>
        <Main />
      </div>
    </Router>
  );
}

export default App;
