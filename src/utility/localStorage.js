import { toast } from "react-toastify";

export const initialTags = () => {
  if (!window.localStorage.getItem("tags")) {
    window.localStorage.setItem("tags", "[]");
  }
};

export const handleTags = (id, title, userId) => {
  initialTags();
  let tags = JSON.parse(window.localStorage.getItem("tags"));
  let idIndex = tags.indexOf({id, title, userId});
  if (idIndex < 0) {
    tags.push({id, title});
    toast("post added to tags list");
  } else {
    tags.splice(idIndex, 1);
    toast("post deleted from tags list");
  }
  localStorage.setItem("tags", JSON.stringify(tags));
};

export const getTags = () => {
    initialTags();
    let tags = JSON.parse(window.localStorage.getItem("tags"));
    console.log(tags)
return tags
};
